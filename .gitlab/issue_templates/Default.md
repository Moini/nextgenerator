<!--
    Writing a good bug report will ensure I'll be able to help efficiently. 🙂
-->

#### Make sure it's not already documented:

* [ ] I have read the [README](https://gitlab.com/Moini/nextgenerator/-/blob/master/README.md) file
* [ ] I have read the extension's Help tab
* [ ] They did not contain any information that helped me fix my problem

#### Summary:
<!-- Summarize the issue/suggestion concisely: -->

... (write here)

#### Steps to reproduce:
<!-- Describe what you did (step-by-step) so I can reproduce: -->

- open Inkscape
- ...

#### Relevant files:

* [ ] SVG file:
* [ ] CSV file:
* [ ] Extension settings screenshot or as text:

#### What happened?

...

<!-- Add any relevant files, too -->

#### What should have happened?

...


#### Version info

(see 'About' tab)


<!--
    ❤️ Thank you for filling in a new bug report, I appreciate the help! ❤️
    Please be patient while I try to find the time to look into your issue.
    Remember that this extension is developed by volunteers in their spare time, and issues are only reviewed by me - I'll try my best to respond to all reports.
-->
